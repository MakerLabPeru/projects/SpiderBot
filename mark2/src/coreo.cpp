#include"coreo.hpp"


byte servo_pin_matrix[NUM_LEGS][NUM_JOINTS] = {
  {0, 1, 2}, {4, 5, 6},
  {8, 9, 10}, {12, 13, 14},
};

Adafruit_PWMServoDriver servo_driver = Adafruit_PWMServoDriver(0x40);

float servo_cal_matrix[NUM_LEGS][NUM_JOINTS][2] = {
  { {90, 45}, {20 ,  90}, {90,  90} }, { {90 , -45}, {160, -90}, {90 , -90} },
  { {90 , 45}, {160, -90}, {90, -90} }, { {90, -45}, {20 ,  90}, {90 ,  90} },
};

SpiderPosition rest_position = {{
    {{0, 1, 0.2}}, {{0, 1, 0.2}},
    {{0, 1, 0.2}}, {{0, 1, 0.2}},
  }};

SpiderPosition straight_position = {{
    {{0, 0, 1}}, {{0,  0, 1}},
    {{0, 0, 1}}, {{0,  0, 1}},
  }};

void setup_legs() {
  servo_driver.begin();
  servo_driver.setPWMFreq(60);
  delay(10);
  write_position(rest_position);
}

void write_position(SpiderPosition position) {
  Log.verbose("[Coreo] Write position");
  for(int i=0; i < NUM_LEGS; i++)
    for(int j=0; j < NUM_JOINTS; j++) {
      float c0 = servo_cal_matrix[i][j][0];
      float c1 = servo_cal_matrix[i][j][1];
      float pos = c0 + position.legs[i].joints[j] * c1;
      float pulse_len = map(pos, 0, 180, 500, 2100);
      float pwm = map(pulse_len, 0, 16000, 0, 4096);
      Log.verbose("[Coreo] Write: [%d][%d] = %F === %F u",i, j, pos, pulse_len);
      servo_driver.setPin(servo_pin_matrix[i][j], pwm);
    }
}

/**
 * Creates an intermediate state between two positions
 */
SpiderPosition interpolate_position(SpiderPosition start_pos,
                                    SpiderPosition end_pos,
                                    float proportion) {
  SpiderPosition position;
  for(int i=0; i < NUM_LEGS; i++)
    for(int j=0; j < NUM_JOINTS; j++) {
      float start = start_pos.legs[i].joints[j];
      float end = end_pos.legs[i].joints[j];

      position.legs[i].joints[j] = start + (end-start) * proportion;
    }
  return position;
}

void write_coreo(SpiderCoreo coreo, float step) {
  float whole_step_float;
  float dec_step = std::modf(step, &whole_step_float);
  int whole_step = static_cast<int>(whole_step_float);
  SpiderPosition start = coreo.steps[whole_step % coreo.length];
  SpiderPosition end = coreo.steps[(whole_step + 1) % coreo.length];
  SpiderPosition new_position;
  Log.verbose("[Coreo] write coreo: whole_step=%d dec_step=%F",
              whole_step, dec_step);
  if (coreo.interpolate && dec_step > 0.05) {
    new_position = interpolate_position(start, end, dec_step);
  } else {
    new_position = start;
  }

  write_position(new_position);
}


void coreo_setup(CoreoCollection& collection, SpiderPlayState& play) {
  play.coreo = &collection.rest;
  play.step = 0;
  play.direction = +1;
  play.last_time = 0;
}

void coreo_loop(SpiderPlayState& play) {
  if (play.direction == 0) {
    // No nos debemos mover
    return;
  }
  unsigned long now = millis();

  if (now > play.last_time + COREO_LOOP_INTERVAL) {
    Log.verbose("[Coreo] Coreo loop: name=\"%s\" step=%F",
                play.coreo->name, play.step);
    play.last_time = now;
    play.step += (static_cast<double>(COREO_LOOP_INTERVAL)
                  / play.coreo->step_time) * play.direction;
    play.step = fmod(play.step, play.coreo->length);
    write_coreo(*play.coreo, play.step);
  }
}

bool change_coreo(SpiderPlayState& play, SpiderCoreo& coreo) {
  // Compare by reference
  if (play.coreo == &coreo) {
    return false;
  }
  Log.notice("[Coreo] Changing coreo: %s", coreo.name);
  play.coreo = &coreo;
  play.step = 0;
  play.last_time = 0;
  return true;
}

void coreo_control_loop(CoreoCollection& collection, SpiderPlayState& play) {
 if(Serial.available()) {
    char c = Serial.read();
    switch(c) {
    case 'r': // Reposo
      change_coreo(play, collection.rest);
      play.direction = +1;
      break;
    case 'd': // Dance
      change_coreo(play, collection.dance);
      play.direction = +1;
      break;
    case 'f': // Avanzar
      change_coreo(play, collection.walk);
      play.direction = +1;
      break;
    case 'b': // Retroceder
      change_coreo(play, collection.walk);
      play.direction = -1;
      break;
    case 's': // detenerse
      play.direction = 0;
      break;
    }
  }
}

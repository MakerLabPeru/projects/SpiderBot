#include "coreoLoader.hpp"

#define COREO_STEP_BUFFER_LEN 30

SpiderPosition coreoStepBuffer[COREO_STEP_BUFFER_LEN];

bool load_coreos(CoreoCollection& collection, const char* filename) {
  File file = SPIFFS.open(filename, "r");
  if (!file) {
    Log.fatal("[CoreoLoader] Failed to open coreos file");
    return false;
  }

  size_t size = file.size();
  if (size > 2048) {
    Log.fatal("[CoreoLoader] Config file size is too large.");
    return false;
  }

  // This file is very big. Load in the heap to avoid an stack overflow
  DynamicJsonBuffer buffer(4000);

  JsonObject& json = buffer.parseObject(file);
  file.close();

  if (!json.success()) {
    Log.fatal("[CoreoLoader] Failed to parse coreos file");
    return false;
  }

  Log.notice("[CoreoLoader] Coreos file loaded");
  Log.verbose("[CoreoLoader] Coreos file size: %d bytes", buffer.size());
  init_collection_struct(collection, json);
  return true;
}

SpiderCoreo coreo_factory(const char* coreo_name,
                          JsonObject& json,
                          SpiderPosition* step_buf,
                          int buf_len) {
  SpiderCoreo coreo;
  strlcpy(coreo.name, coreo_name, sizeof(coreo.name));
  coreo.step_time = json["stepTime"];
  coreo.interpolate = json["interpolate"];
  coreo.steps = step_buf;
  JsonArray& steps = json["steps"].as<JsonArray>();
  coreo.length = steps.size();

  for (unsigned int step_idx=0; step_idx<steps.size(); step_idx++) {
    SpiderPosition& position = step_buf[step_idx];
    for(int leg_idx=0; leg_idx<NUM_LEGS; leg_idx++) {
      for (int joint_idx=0; joint_idx<NUM_JOINTS; joint_idx++) {
        position.legs[leg_idx].joints[joint_idx] =
          steps[step_idx][leg_idx][joint_idx];
      }
    }
  }
  Log.verbose("[CoreoLoader] Loading coreo: name=\"%s\" step_time=%d",
              coreo_name, coreo.step_time);

  return coreo;
};

void init_collection_struct(CoreoCollection& collection, JsonObject& json) {
  SpiderCoreo coreo;
  SpiderPosition *buf = coreoStepBuffer;
  int buf_len = COREO_STEP_BUFFER_LEN;

  struct NamedIter {
    char name[20];
    SpiderCoreo& coreo;
  };

  NamedIter iter[] = {
    {"rest", collection.rest},
    {"walk", collection.walk},
    {"dance", collection.dance},
  };

  for (int i=0; i < arrayLength(iter); i++) {
    char *name = iter[i].name;
    coreo = coreo_factory(name, json[name], buf, buf_len);
    buf += coreo.length;
    buf_len -= coreo.length;
    iter[i].coreo = coreo;
  }
}

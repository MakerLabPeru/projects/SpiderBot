#pragma once

#include <cmath>
#include <Arduino.h>
#include <ArduinoLog.h>
#include <Adafruit_PWMServoDriver.h>

#define arrayLength(array) (sizeof((array))/sizeof((array)[0]))

#define NUM_LEGS 4
#define NUM_JOINTS 3
/**
 * Tiempo entre cada iteracion del bucle de escritura de servos
 */
#define COREO_LOOP_INTERVAL 100

/*
 * Orden de patas:
 * - izquierda, atras
 * - derecha, atras
 * - izquierda, adelante
 * - derecha, adelante
 *
 * Orden de articulaciones:
 * - Interior
 * - Exterior
 */
extern byte servo_pin_matrix[NUM_LEGS][NUM_JOINTS];

// Maps the coreography positions to real servo degrees
// (constant and linear coefficients)
extern float servo_cal_matrix[NUM_LEGS][NUM_JOINTS][2];

extern Adafruit_PWMServoDriver servo_driver;

typedef struct {
  float joints[NUM_JOINTS];
} LegPosition;

typedef struct {
  LegPosition legs[NUM_LEGS];
} SpiderPosition;

void setup_legs();

extern SpiderPosition rest_position;

extern SpiderPosition straight_position;

extern SpiderPosition rest_coreo[];
extern int rest_coreo_len;

extern SpiderPosition walk_coreo[];
extern int walk_coreo_len;

extern SpiderPosition dance_coreo[];
extern int dance_coreo_len;

struct SpiderCoreo{
  SpiderPosition *steps;
  char name[15];
  int step_time;
  bool interpolate;
  int length;
};

/**
 * Almacena el estado de la ejecución de la coreografia
 */
struct SpiderPlayState {
  SpiderCoreo *coreo;
  double step;
  int direction;
  unsigned long last_time;
};

struct CoreoCollection {
  SpiderCoreo rest, walk, dance;
};


void write_position(SpiderPosition position);
void write_coreo(SpiderCoreo coreo, float step);
SpiderPosition interpolate_position(SpiderPosition start_pos,
                                    SpiderPosition end_pos,
                                    float proportion);

void coreo_setup(CoreoCollection& collection, SpiderPlayState& play);
void coreo_loop(SpiderPlayState& play);
bool change_coreo(SpiderPlayState& play, SpiderCoreo& coreo);
void coreo_control_loop(CoreoCollection& collection, SpiderPlayState& play);

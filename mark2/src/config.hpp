#pragma once

#include <Arduino.h>
#include <ArduinoLog.h>
#include <ArduinoJson.h>

#include <FS.h>

typedef struct {
  char ssid[30];
   char password[65];
} WifiConfig;

typedef struct {
  char hostname[20];
  WifiConfig wifi;
} Config;

bool load_config(Config& config, const char* filename);

void init_config_struct(Config& config, JsonObject& json);

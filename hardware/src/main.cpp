#include "main.hpp"

Config config;
CoreoCollection collection;
SpiderPlayState play;

void log_suffix(Print* output) {
  output->print("\n");
}

void setup() {
  // Serial setup
  Serial.begin(115200);
  Serial.println();
  Log.begin(LOG_LEVEL_VERBOSE, &Serial);
  Log.setSuffix(log_suffix);
  Log.notice("[Main] Booting");
  // Data files loading
  Log.notice("[Main] Mounting SPIFFS");
  if(!SPIFFS.begin()) {
    Log.fatal("[Main] Error mounting SPIFFS");
  }
  if (!load_config(config, "/config.json")
      || !load_coreos(collection, "/coreo.json")) {
    delay(500);
    ESP.restart();
  }
  SPIFFS.end();
   // Network setup (Do not wait for connection)
  wifi_setup(config);
  ota_setup(config);
  setup_legs();
  coreo_setup(collection, play);
  Log.notice("[Main] Ready to receive orders.");
}

void loop() {
  ota_loop();
  coreo_loop(play);
  coreo_control_loop(collection, play);
  health_loop();
}

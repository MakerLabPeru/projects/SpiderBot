#pragma once

#include <functional>
#include <Arduino.h>
#include <ArduinoLog.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "config.hpp"

#include "FS.h"

void ota_setup(Config config);

void ota_loop();

extern bool is_network_ready();

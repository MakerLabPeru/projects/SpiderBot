#include "health.hpp"

void health_loop() {
  static unsigned long last_time = 0;
  unsigned long now = millis();
  if (now > last_time + 10000) {
    Log.verbose("[Health] Free heap: %d", ESP.getFreeHeap());
    last_time = now;
  }
}

#include "config.hpp"

bool load_config(Config& config, const char* filename) {
  File configFile = SPIFFS.open(filename, "r");
  if (!configFile) {
    Log.fatal("[Config] Failed to open config file");
    return false;
  }

  size_t size = configFile.size();
  if (size > 1024) {
    Log.fatal("[Config] Config file size is too large.");
    return false;
  }

  StaticJsonBuffer<200> buffer;

  JsonObject& json = buffer.parseObject(configFile);
  configFile.close();

  if (!json.success()) {
    Log.fatal("[Config] Failed to parse config file");
    return false;
  }

  init_config_struct(config, json);

  return true;
}

void init_config_struct(Config& config, JsonObject& json) {
  strlcpy(config.hostname, json["hostname"] | "", sizeof(config.hostname));
  strlcpy(config.wifi.ssid, json["wifi"]["ssid"], sizeof(config.wifi.ssid));
  strlcpy(config.wifi.password, json["wifi"]["password"], sizeof(config.wifi.password));

  Log.verbose("[Config] Loaded \"hostname\": \"%s\"", config.hostname);
  Log.verbose("[Config] Loaded \"wifi.ssid\": \"%s\"", config.wifi.ssid);
}

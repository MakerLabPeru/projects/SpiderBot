#include "wifi.hpp"

IPAddress dns(8, 8, 8, 8);

void wifi_setup(Config config) {
  // Avoid writing the WIFI SSID and password to flash memory
  // to avoid early memory wearing
  WiFi.persistent(false);
  WiFi.hostname(config.hostname);
  Log.notice("[WiFi] Hostname: \"%s\"", config.hostname);
  Log.notice("[WiFi] Configuring Wifi");
  WiFi.mode(WIFI_STA);
  WiFi.begin(config.wifi.ssid, config.wifi.password);

  Log.notice("[WiFi] Connecting to \"%s\" [RSSI: %d]",
             WiFi.SSID().c_str(), WiFi.RSSI());
}

bool is_network_ready() {
  return WiFi.waitForConnectResult() == WL_CONNECTED;
}

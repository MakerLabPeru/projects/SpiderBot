#include "ota.hpp"

void ota_setup(Config config) {
  ArduinoOTA.setHostname(config.hostname);

  ArduinoOTA.onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH) {
        type = "sketch";
      } else { // U_SPIFFS
        type = "filesystem";
        SPIFFS.end();
      }
      Log.notice("[OTA] Start updating %s", type.c_str() );
    });
  ArduinoOTA.onEnd([]() {
      Log.notice("[OTA] End");
    });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      static int last = 0;
      int current = progress*100/total;
      if (current % 10 == 0 && current != last) {
        Log.notice("[OTA] Progress: %d%%", current);
        last = current;
      }
    });
  ArduinoOTA.onError([](ota_error_t error) {
      String error_msg;
      if (error == OTA_AUTH_ERROR) {
        error_msg = "Auth Failed";
      } else if (error == OTA_BEGIN_ERROR) {
        error_msg = "Begin Failed";
      } else if (error == OTA_CONNECT_ERROR) {
        error_msg = "Connect Failed";
      } else if (error == OTA_RECEIVE_ERROR) {
        error_msg = "Receive Failed";
      } else if (error == OTA_END_ERROR) {
        error_msg = "End Failed";
      }
      Log.fatal("[OTA] Upload error[%d]: %s", error, error_msg.c_str());
    });
  ArduinoOTA.begin();
  Log.notice("[OTA] Ready");
}

void ota_loop() {
  ArduinoOTA.handle();
}

#pragma once

#include <Arduino.h>
#include <ArduinoLog.h>
#include <ArduinoJson.h>

#include <FS.h>

#include "coreo.hpp"

bool load_coreos(CoreoCollection& collection, const char* filename);

void init_collection_struct(CoreoCollection& collection, JsonObject& json);
